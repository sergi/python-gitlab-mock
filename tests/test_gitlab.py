#!/usr/bin/env python3.9

# Copyright (C) 2023 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2023 Collabora Ltd"

from abstract import GitlabTestCase
import environment
from mockgitlab import Gitlab
from mockgitlab.exceptions import GitlabGetError
from mockgitlab.branches import ProjectBranch
from mockgitlab.projects import Project
from mockgitlab.generators import (create_random_fake_token,
                                   create_random_fake_sha1_hash)
from pytest import raises
from random import randint
from unittest import main


def _gitlab_builder(url, private_token=None):
    return Gitlab(url, private_token=private_token)


class TestGitlab(GitlabTestCase):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._prepare_logger("TestGitlab")

    def __build_gitlab_object(self):
        fake_token = create_random_fake_token()
        self._prepare_environment({'GITLAB_TOKEN': fake_token})
        # force to create the build of the fake
        environment._gitlab_builder = _gitlab_builder
        return environment.build_gitlab_object()

    # absorbed by test_get_gitlab_project_objects
    # def test_build_gitlab_object(self):
    #     gl = self.__build_gitlab_object()

    def test_get_gitlab_project_objects(self):
        gl = self.__build_gitlab_object()
        for namespace, project_name in [('virgl', 'virglrenderer'),
                                        ('mesa', 'mesa'),
                                        ('mesa', 'piglit')]:
            project = gl.projects.get(f"gfx-ci-bot/{project_name}")
            self.assertIsInstance(project, Project)
            project = gl.projects.get(f"{namespace}/{project_name}")
            self.assertIsInstance(project, Project)
        with raises(AssertionError):
            project = gl.projects.get(f"{namespace}/doesntexists")

    def __prepare_gl_project(self, namespace="gfx-ci-bot",
                             project_name="virglrenderer"):
        gl = self.__build_gitlab_object()
        project = gl.projects.get(f"{namespace}/{project_name}")
        return gl, project

    def test_get_branch(self):
        gl, project = self.__prepare_gl_project()
        gl.debug("> get_branch()")
        branch = project.branches.get("feature_request")
        self.assertIsInstance(branch, ProjectBranch)
        gl.debug("< get_branch()")
        with raises(GitlabGetError):
            branch = project.branches.get("doesntexists")
            branch = project.branches.create("doesntexists")

    def test_get_commit(self):
        gl, project = self.__prepare_gl_project()
        gl.debug("> get_commit()")
        commit = project.commits.get(create_random_fake_sha1_hash())
        gl.debug("< get_commit()")

    def test_get_file(self):
        gl, project = self.__prepare_gl_project()
        gl.debug("> get_file()")
        file = project.files.get(".gitlab-ci.yml",
                                 ref=create_random_fake_sha1_hash())
        gl.debug("< get_file()")

    def test_get_issue(self):
        gl, project = self.__prepare_gl_project()
        gl.debug("> get_issue()")
        issue = project.issues.get(randint(int(1e3), int(2e4)))
        gl.debug("< get_issue()")

    def test_get_job(self):
        gl, project = self.__prepare_gl_project()
        gl.debug("> get_job()")
        job = project.jobs.get(randint(int(1e8), int(2e9)))
        with raises(GitlabGetError):
            job.artifact('bar/foo/doesntexists.csv')
        fake_artifact_name = 'result/results.csv'
        fake_artifact_content = \
            b'fast_color_clear@all-colors,Pass,0.20239443\n' \
            b'fast_color_clear@fast-slow-clear-interaction,Pass,0.1916847\n' \
            b'fast_color_clear@fcc-blit-between-clears,Pass,0.34520018\n'
        job.set_artifact(fake_artifact_name, fake_artifact_content)
        self.assertEqual(job.artifact(fake_artifact_name),
                         fake_artifact_content)
        gl.debug("< get_job()")

    def test_get_mergerequest(self):
        gl, project = self.__prepare_gl_project()
        gl.debug("> get_mergerequest()")
        mergerequest = project.mergerequests.get(randint(int(1e2), int(2e3)))
        gl.debug("< get_mergerequest()")

    def test_get_pipeline(self):
        gl, project = self.__prepare_gl_project()
        gl.debug("> get_pipeline()")
        pipeline = project.pipelines.get(randint(int(1e4), int(2e5)))
        gl.debug("< get_pipeline()")


if __name__ == '__main__':
    main()
