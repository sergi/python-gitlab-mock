#!/usr/bin/env python3.9

# Copyright (C) 2023 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2023 Collabora Ltd"

from mockgitlab import Gitlab
import os

__gl_fdo_url = 'https://gitlab.freedesktop.org'


def _gitlab_builder(url, private_token=None):
    return Gitlab(url, private_token=private_token)


def build_gitlab_object():
    gitlab_token = os.environ.get('GITLAB_TOKEN')
    if gitlab_token is None:
        try:
            with open(os.path.expanduser('~/.gitlab-token')) as file_descriptor:
                gitlab_token = file_descriptor.read().strip()
        except FileNotFoundError:
            raise AssertionError(
                f"Use the GITLAB_TOKEN environment variable or "
                f"the ~/.gitlab-token to provide this information")
    gl = _gitlab_builder(url=__gl_fdo_url, private_token=gitlab_token)
    gl.auth()
    return gl
