#!/usr/bin/env python3.9

# Copyright (C) 2023 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2023 Collabora Ltd"

from datetime import datetime
from .exceptions import GitlabGetError
from .generators import create_random_fake_sha1_hash
from random import random, randint
from unittest import mock


class ProjectCommit(mock.MagicMock):
    __commit_hash = None
    __parent = None
    __gl = None

    __branch = None
    __created_at = None
    __last_pipeline = None
    __message = None

    def __init__(self, dct=None, _parent=None, _gl=None,
                 *args, **kwargs):
        super().__init__(*args, **kwargs)
        if dct is not None:
            if 'sha' in dct:
                self.__commit_hash = dct['sha']
            else:
                self.__commit_hash = create_random_fake_sha1_hash()
            if 'branch' in dct:
                self.__branch = dct['branch']
            if 'commit_message' in dct:
                self.__message = dct['commit_message']
        self.__parent = _parent
        self.__gl = _gl
        self.__created_at = datetime.now()
        self.__gl.debug(f"Build a commit {self.__commit_hash!r} "
                        f"in branch {self.__branch!r}")

    @property
    def id(self):
        return self.__commit_hash

    @property
    def short_id(self):
        return self.__commit_hash[:8]

    @property
    def sha(self):
        return self.__commit_hash

    @property
    def branch(self):
        return self.__branch

    @property
    def title(self):
        if self.__message.count('\n'):
            return self.__message.split('\n')[0]
        return self.__message

    @property
    def message(self):
        return self.__message

    @property
    def web_url(self):
        return f"{self.__gl.url}{self.__parent.project.path_with_namespace}/" \
               f"-/commit/{self.__commit_hash}"

    @property
    def created_at(self):
        return self.__created_at.strftime('%Y-%m-%dT%H:%M:%S.00+02:00')

    @property
    def last_pipeline(self):
        """
        Return None if not ready, but when ready the dictionary has a key
        with an id
        :return:
        """
        if self.__last_pipeline is None:
            lapsed_time = datetime.now() - self.__created_at
            if lapsed_time.seconds < randint(1, 5):
                # self.__gl.debug(f"In commit {self.sha} simulating pipeline not ready")
                return None  # not ready
            self.__last_pipeline = self.__parent.project.pipelines.create()
            # self.__gl.debug(f"In commit {self.sha} pipeline ready")
        # self.__gl.debug(f"requested the last pipeline of {self.sha} commit")
        return {'id': self.__last_pipeline.id,
                'web_url': self.__last_pipeline.web_url}


class ProjectCommitManager(mock.MagicMock):
    __project = None
    __parent = None
    __gl = None
    __existing_commits = None

    def __init__(self, _project=None, _parent=None, _gl=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__project = _project
        self.__parent = _parent
        self.__gl = _gl
        self.__existing_commits = {}

    @property
    def project(self):
        return self.__project

    def get(self, sha):
        if sha in self.__existing_commits:
            return self.__existing_commits[sha]
        # raise GitlabGetError(response_code=404,
        #                      error_message='404 Commit Not Found')
        args = {'sha': sha,
                'branch': 'fake_branch',
                'commit_message': 'fake commit'}
        return self.__new_commit(args)

    def create(self, *args):
        commit_obj = self.__new_commit(args)
        return commit_obj

    def __new_commit(self, args):
        while isinstance(args, tuple):
            args = args[0]
        # self.__gl.debug(f"Create a commit {list(args.keys())}")
        commit_obj = ProjectCommit(args, self, self.__gl)
        while commit_obj.sha in self.__existing_commits:
            self.__gl.warning(f"Uou! fake sha1 collision XD")
            commit_obj = ProjectCommit(args, self, self.__gl)
        self.__existing_commits[commit_obj.sha] = commit_obj
        return commit_obj
