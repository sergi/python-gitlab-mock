#!/usr/bin/env python3.9

# Copyright (C) 2023 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2023 Collabora Ltd"

from datetime import datetime
from random import randint
from unittest import mock


class ProjectMergeRequest(mock.MagicMock):
    __parent = None
    __gl = None

    __id = None
    __iid = None
    __title = None
    __description = None
    __state = None
    __merge_status = None
    __created_at = None
    __target_branch = None
    __source_branch = None
    __labels = None
    __should_remove_source_branch = None
    __force_remove_source_branch = None
    __head_pipeline = None

    def __init__(self, _parent=None, _gl=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__id = randint(int(1e3), int(1e4))
        self._assign_iid()
        self.__parent = _parent
        self.__gl = _gl
        self.__state = 'opened'
        self.__merge_status = 'can_be_merged'
        self.__created_at = datetime.now()
        self.__gl.debug(f"Build merge request {self.iid} "
                        f"in project {self.__parent!r}")

    def _assign_iid(self):
        self.__iid = randint(int(1e2), int(1e3))

    @property
    def iid(self):
        return self.__iid

    @property
    def web_url(self):
        return f"{self.__gl.url}{self.__parent.project.path_with_namespace}/" \
               f"-/merge_requests/{self.__iid}"

    @property
    def title(self):
        return self.__title

    @title.setter
    def title(self, value):
        self.__title = value

    @property
    def description(self):
        return self.__description

    @description.setter
    def description(self, value):
        self.__description = value

    @property
    def state(self):
        return self.__state

    @property
    def merge_status(self):
        return self.__merge_status

    @property
    def created_at(self):
        return self.__created_at.strftime('%Y-%m-%dT%H:%M:%S.00+02:00')

    @property
    def target_branch(self):
        return self.__target_branch

    @target_branch.setter
    def target_branch(self, value):
        self.__target_branch = value

    @property
    def source_branch(self):
        return self.__source_branch

    @source_branch.setter
    def source_branch(self, value):
        self.__source_branch = value

    @property
    def labels(self):
        return self.__labels

    @labels.setter
    def labels(self, value):
        self.__labels = value

    @property
    def should_remove_source_branch(self):
        return self.__should_remove_source_branch or False

    @should_remove_source_branch.setter
    def should_remove_source_branch(self, value):
        self.__should_remove_source_branch = bool(value)

    @property
    def force_remove_source_branch(self):
        return self.__force_remove_source_branch

    @force_remove_source_branch.setter
    def force_remove_source_branch(self, value):
        self.__force_remove_source_branch = bool(value)

    def save(self):
        pass  # self.__gl.debug(f"called save to {self.web_url}")

    @property
    def head_pipeline(self):
        if self.__head_pipeline is None:
            lapsed_time = datetime.now() - self.__created_at
            if lapsed_time.seconds < randint(1, 5):
                return None
            self.__head_pipeline = self.__parent.project.pipelines.create()
        return {'id': self.__head_pipeline.id}


class ProjectMergeRequestManager(mock.MagicMock):
    __project = None
    __parent = None
    __gl = None
    __mergerequests = None

    def __init__(self, _project=None, _parent=None, _gl=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__project = _project
        self.__parent = _parent
        self.__gl = _gl
        self.__mergerequests = {}

    @property
    def project(self):
        return self.__project

    def get(self, _iid):
        if _iid in self.__mergerequests:
            return self.__mergerequests[_iid]
        # FIXME: perhaps a "doesn't exist" exception
        return self.__new_mergerequest()

    def create(self, *args):
        mergerequest_obj = self.__new_mergerequest()
        mergerequest_obj.title = args[0]['title']
        mergerequest_obj.source_branch = args[0]['source_branch']
        mergerequest_obj.target_branch = args[0]['target_branch']
        mergerequest_obj.labels = args[0]['labels']
        return mergerequest_obj

    def __new_mergerequest(self):
        mergerequest_obj = ProjectMergeRequest(self, self.__gl)
        while mergerequest_obj.iid in self.__mergerequests.keys():
            mergerequest_obj._assign_iid()
        self.__mergerequests[mergerequest_obj.iid] = mergerequest_obj
        return mergerequest_obj
