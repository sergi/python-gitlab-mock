#!/usr/bin/env python3.9

# Copyright (C) 2023 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2023 Collabora Ltd"

from unittest import mock


class ProjectIssue(mock.MagicMock):
    __id = None
    __parent = None
    __gl = None

    def __init__(self, _id=None, _parent=None, _gl=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__id = _id
        self.__parent = _parent
        self.__gl = _gl
        self.__gl.debug(f"Build an issue {self.__id} "
                        f"is project {self.__parent!r}")


class ProjectIssueManager(mock.MagicMock):
    __project = None
    __parent = None
    __gl = None

    def __init__(self, _project=None, _parent=None, _gl=None, *args, **kwargs):
        super().__init__()
        self.__project = _project
        self.__parent = _parent
        self.__gl = _gl

    @property
    def project(self):
        return self.__project

    def get(self, name):
        return ProjectIssue(name, self, self.__parent)
