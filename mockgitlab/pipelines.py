#!/usr/bin/env python3.9

# Copyright (C) 2023 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2023 Collabora Ltd"

from .generators import (create_random_fake_sha1_hash, mesa_jobs_generator,
                         virgl_jobs_generator)
from unittest import mock
from random import choices, randint
from .status import PipelineStatus


class ProjectPipeline(mock.MagicMock):
    __id = None
    __parent = None
    __gl = None
    __pipeline_jobs = None

    def __init__(self, _parent=None, _gl=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__id = randint(int(8e5), int(9e5))
        self.__parent = _parent
        self.__gl = _gl
        project_name = self.__parent.project.name
        if project_name in ['virglrenderer', 'mesa']:
            self.__gl.debug(f"Build a pipeline for {project_name!r} project "
                            f"(id {self.__id})")
            if project_name in ['virglrenderer']:
                self.__pipeline_jobs = virgl_jobs_generator(
                    self.__parent.project.jobs, self.__gl)
            elif project_name in ['mesa']:
                self.__pipeline_jobs = mesa_jobs_generator(
                    self.__parent.project.jobs, self.__gl)

    @property
    def id(self):
        return self.__id

    @property
    def web_url(self):
        return f"{self.__gl.url}{self.__parent.project.path_with_namespace}/" \
               f"-/pipelines/{self.__id}"

    @property
    def sha(self):
        return create_random_fake_sha1_hash()

    @property
    def status(self):
        results = self.__collect_jobs_status()
        if results is not None:
            status = self.__decide_pipeline_status(*results)
            if status is not None:
                return status
        return 'success'

    @property
    def user(self):
        return {'username': choices(['marge-bot', 'bar', 'foo'], k=1)[0]}

    @property
    def jobs(self):
        return self.__parent.project.jobs

    def __collect_jobs_status(self):
        try:
            if self.__pipeline_jobs is not None:
                created, running, success, failed, manual = [0] * 5
                for job in self.__pipeline_jobs.values():
                    if job.status in ['created']:
                        created += 1
                    elif job.status in ['running']:
                        running += 1
                    elif job.status in ['success']:
                        success += 1
                    elif job.status in ['failed']:
                        failed += 1
                    elif job.status in ['manual']:
                        manual += 1
                return created, running, success, failed, manual
            else:
                self.__gl.debug(f"In pipeline {self.__id}, no jobs to collect "
                                f"their status")
        except Exception as exception:
            self.__gl.error(f"Collecting jobs status: {exception}")

    def __decide_pipeline_status(self, created, running, success, failed,
                                 manual):
        if running != 0:
            return 'running'
        if manual != 0:
            if failed != 0:
                return 'failed'
            if success != 0:
                return 'blocked'
        if created != 0:
            return 'pending'
        if failed != 0:
            return 'failed'


class ProjectPipelineManager(mock.MagicMock):
    __project = None
    __parent = None
    __gl = None
    __pipeline_ids = None

    def __init__(self, _project=None, _parent=None, _gl=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__project = _project
        self.__parent = _parent
        self.__gl = _gl
        self.__pipeline_ids = {}

    @property
    def project(self):
        return self.__project

    @property
    def path(self):
        return f"/projects/{self.__project.id}/pipelines"

    def get(self, _id):
        if _id in self.__pipeline_ids:
            return self.__pipeline_ids[_id]
        return self.__new_pipeline()

    def create(self, *args):
        # self.__gl.debug(f"Request to create a pipeline in project "
        #                 f"{self.project.path_with_namespace}")
        return self.__new_pipeline()

    def list(self, per_page=None, page=None):
        def _generate_pipeline_list(size=None):
            if size is None:
                size = randint(3, 10)
            _id = randint(int(1e5), int(6e5))
            pipelines = [self.__new_pipeline()]
            while len(pipelines) < size:
                if _id == int(1e4):
                    break
                _id = randint(int(1e5), _id)
                pipelines.append(self.__new_pipeline())
            return pipelines
        return _generate_pipeline_list(size=per_page)

    def __new_pipeline(self):
        pipeline_obj = ProjectPipeline(self, self.__gl)
        while pipeline_obj.id in self.__pipeline_ids:
            pipeline_obj = ProjectPipeline(self, self.__gl)
        self.__pipeline_ids[pipeline_obj.id] = pipeline_obj
        return pipeline_obj
