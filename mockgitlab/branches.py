#!/usr/bin/env python3.9

# Copyright (C) 2023 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2023 Collabora Ltd"

from .generators import create_random_fake_sha1_hash
from .exceptions import GitlabGetError
from random import random
from re import search as re_search
from unittest import mock


class ProjectBranch(mock.MagicMock):
    __branch_name = None
    __parent = None
    __gl = None

    def __init__(self, _branch_name=None, _parent=None, _gl=None,
                 *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__branch_name = _branch_name
        self.__parent = _parent
        self.__gl = _gl
        if self.__parent is not None:
            args = {'sha': create_random_fake_sha1_hash(),
                    'branch': self.__branch_name,
                    'commit_message': 'fake commit'}
            self.__last_commit = self.__parent.project.commits.create(args)
            self.__gl.debug(f"Build a branch {self.__branch_name!r} "
                            f"with a last commit {self.__last_commit.sha!r}")

    @property
    def name(self):
        return self.__branch_name

    @property
    def web_url(self):
        return f"{self.__gl.url}{self.__parent.project.path_with_namespace}/" \
               f"-/tree/{self.__branch_name}"

    @property
    def commit(self):
        return {'id': self.__last_commit.sha}

    def delete(self):
        self.__gl.debug(f"requested to delete {self.__branch_name} branch")


class ProjectBranchManager(mock.MagicMock):
    __project = None
    __parent = None
    __gl = None
    __branches = None

    def __init__(self, _project=None, _parent=None, _gl=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__project = _project
        self.__parent = _parent
        self.__gl = _gl
        self.__branches = {}

    @property
    def project(self):
        return self.__project

    def get(self, name):
        return self.__build_branch(name)

    def create(self, *args):
        name = args[0]['branch']
        if name in self.__branches:
            exception = GitlabGetError()
            exception.response_code = 400
            exception.error_message = 'Branch already exists'
            raise exception
        return self.__build_branch(name)

    def __build_branch(self, name):
        if name in self.__branches:
            return self.__branches[name]
        if name in ['doesntexists']:
            exception = GitlabGetError()
            exception.response_code = 404
            exception.error_message = '404 Branch Not Found'
            raise exception
        branch_obj = ProjectBranch(name, self, self.__gl)
        self.__branches[name] = branch_obj
        return branch_obj
