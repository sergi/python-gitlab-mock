#!/usr/bin/env python3.9

# Copyright (C) 2023 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2023 Collabora Ltd"

from .generators_mesa import mesa_jobs_builder
from random import choices, sample
from string import ascii_letters, ascii_lowercase, digits


def create_random_fake_token():
    letters = choices(ascii_letters, k=20)
    numbers = choices(digits, k=20)
    return f"glpat-{''.join(sample(letters + numbers, 20))}"


def create_random_fake_sha1_hash():
    letters = choices("abcdef", k=40)
    numbers = choices(digits, k=40)
    return f"{''.join(sample(letters + numbers, 40))}"


def _jobs_dependency_relation(first, then):
    # avoid circular dependency
    if first in then.trigger_jobs or then in first.dependency_jobs:
        raise LookupError(f"Attempt to create a circular dependency from "
                          f"{then.name} to {first.name}")
    if then not in first.trigger_jobs:
        first.trigger_jobs.append(then)
    if first not in then.dependency_jobs:
        then.dependency_jobs.append(first)


def virgl_jobs_generator(parent, gl):
    build_job = parent.get('debian/x86_build')
    build_job.stage = 'build'
    jobs = [build_job]
    # gl.debug(f"created {build_job.name!r} job")
    for job_name in ['make check venus', 'make check trace-stderr',
                     'make check clang-fuzzer', 'mesa check meson']:
        job = parent.get(job_name)
        job.stage = 'sanity test'
        jobs.append(job)
        # gl.debug(f"created {job.name!r} job")
    test_stage_job_names = [f"{a}-{b}-{c}" for a in ['deqp', 'piglit']
                            for b in ['gl', 'gles'] for c in ['host', 'virt']]
    for job_name in test_stage_job_names:
        if job_name.startswith('piglit') and job_name.endswith('virt'):
            for i in range(1, 4):
                job = parent.get(f"{job_name} {i}/3")
                job.stage = 'test'
                jobs.append(job)
                _jobs_dependency_relation(build_job, job)
                # gl.debug(f"created {job.name!r} job")
        else:
            job = parent.get(job_name)
            job.stage = 'test'
            jobs.append(job)
            _jobs_dependency_relation(build_job, job)
            # gl.debug(f"created {job.name!r} job")
    gl.debug(f"generated {parent.project.path_with_namespace} jobs")
    return jobs


def mesa_jobs_generator(parent, gl):
    # gl.debug(f"*** mesa_jobs_generator() ***")

    def _pick_one_of_the_manual_jobs():
        return choices(_manual_jobs, k=1)[0]

    def _build_job(builder, name, stage, status, unlock, depends_on):
        job_obj = builder.get(name)
        job_obj.stage = stage
        job_obj.set_status(status)
        jobs[name] = job_obj
        if unlock is not None:
            job_obj.unlock_status(unlock)
        if depends_on is None:
            pass  # gl.debug(f"{_job_name} is an initial job")
        elif len(depends_on) == 0:
            if len(jobs) > 0:
                while len(job_obj.dependency_jobs) == 0:
                    try:
                        random_dependency = _pick_one_of_the_manual_jobs()
                        _jobs_dependency_relation(random_dependency, job_obj)
                    except LookupError as exception:
                        gl.warning(f"avoided circular dependency: {exception}")
                try:
                    gl.debug(f"random dependency for {_job_name}: "
                             f"{random_dependency.name}")
                except:
                    gl.warning(
                        f"{job_obj.name} has "
                        f"{[job.name for job in job_obj.dependency_jobs]} "
                        f"dependencies")
            else:
                gl.debug(f"Not ready to setup dependencies for {job_name}")
                if '*' not in _pending_dependencies:
                    _pending_dependencies['*'] = []
                _pending_dependencies['*'].append(_job_name)
        else:
            for dependency in depends_on:
                if dependency in jobs:
                    _jobs_dependency_relation(jobs[dependency], job_obj)
                else:
                    if dependency not in _pending_dependencies:
                        _pending_dependencies[dependency] = []
                    _pending_dependencies[dependency].append(name)
            # gl.debug(f"{_job_name} depends on {depends_on}")

    def _build_job_with_details(builder, name, stage, status, details):
        if isinstance(_job_details, dict):
            if 'unlock' in details:
                unlock = details['unlock']
            else:
                unlock = None
            if 'depends_on' in details:
                depends_on = details['depends_on']
                if not isinstance(depends_on, list):
                    depends_on = []
            else:
                depends_on = None
            if 'number' in details:
                _number = details['number']
                for i in range(1, _number+1):
                    _sub_name = f"{name} {i}/{_number}"
                    _build_job(builder, _sub_name, stage, status, unlock,
                               depends_on)
                # gl.debug(f"build {_number} instances of {_job_name}")
            else:
                _build_job(builder, name, stage, status, unlock, depends_on)
                # gl.debug(f"build {_job_name} job")
        else:
            _build_job(builder, name, stage, status, None, None)
            # gl.debug(f"build {_job_name} job")
        return jobs

    jobs = {}
    _manual_jobs = []
    _pending_dependencies = {}
    for _job_initial_status, _stages in mesa_jobs_builder.items():
        for _job_stage, _job in _stages.items():
            for _job_name, _job_details in _job.items():
                _build_job_with_details(parent, _job_name, _job_stage,
                                        _job_initial_status, _job_details)
                if _job_initial_status == 'manual':
                    _manual_jobs.append(jobs[_job_name])
    if len(_pending_dependencies) != 0:
        for job_name, dependants in _pending_dependencies.items():
            gl.debug(f"Time to review job dependencies for {job_name}")
            for dependant in dependants:
                if job_name == '*':
                    while len(jobs[dependant].dependency_jobs):
                        try:
                            random_dependency = _pick_one_of_the_manual_jobs()
                            _jobs_dependency_relation(jobs[random_dependency],
                                                      jobs[dependant])
                        except LookupError as exception:
                            gl.warning(f"avoided circular dependency: "
                                       f"{exception}")
                    gl.debug(f"random dependency for {_job_name}: "
                             f"{random_dependency.name}")
                else:
                    _jobs_dependency_relation(jobs[job_name], jobs[dependant])
    gl.debug(f"*** Build {len(jobs)} jobs to simulate mesa pipeline ***")
    # for job_name, job_obj in jobs.items():
    #     gl.debug(f"job {job_name} dependencies: "
    #              f"{[job.name for job in job_obj.dependency_jobs]}")
    return jobs
