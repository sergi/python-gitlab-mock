#!/usr/bin/env python3.9

# Copyright (C) 2023 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2023 Collabora Ltd"

from .branches import ProjectBranchManager
from .commits import ProjectCommitManager
from .files import ProjectFileManager
from .issues import ProjectIssueManager
from .jobs import ProjectJobManager
from .knownprojects import known_projects
from .mergerequests import ProjectMergeRequestManager
from .pipelines import ProjectPipelineManager
from unittest import mock
from random import randint


class Project(mock.MagicMock):
    __path_with_namespace: str = None
    __project_namespace: str = None
    __project_name: str = None
    __id: int = None
    __default_branch: str = None

    __parent = None  # ProjectManager
    __gl = None  # Gitlab

    def __init__(self, _path_with_namespace, _parent=None, _gl=None,
                 *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.__path_with_namespace = _path_with_namespace
        self.__project_namespace, self.__project_name = \
            self.__path_with_namespace.rsplit('/', 1)
        if self.__project_name not in list(known_projects.keys()):
            raise AssertionError(f"Unsupported project {self.__project_name}")
        self.__id = randint(int(1e3), int(2e4))
        self.__default_branch = \
            known_projects[self.__project_name]['default_branch']
        self.__parent = _parent
        self.__gl = _gl
        self.__gl.debug(f"Build a gitlab Project object for "
                        f"{self.__project_name} in {self.__project_namespace} "
                        f"namespace")
        self.branches = ProjectBranchManager(self, self.__parent, self.__gl)
        self.commits = ProjectCommitManager(self, self.__parent, self.__gl)
        self.files = ProjectFileManager(self, self.__parent, self.__gl)
        self.issues = ProjectIssueManager(self, self.__parent, self.__gl)
        self.jobs = ProjectJobManager(self, self.__parent, self.__gl)
        self.mergerequests = ProjectMergeRequestManager(self, self.__parent,
                                                        self.__gl)
        self.pipelines = ProjectPipelineManager(self, self.__parent, self.__gl)

    @property
    def name(self):
        return self.__project_name

    def __repr__(self):
        return f"<Project id:{self.id} " \
               f"path_with_namespace:{self.path_with_namespace}>"

    @property
    def path_with_namespace(self):
        return self.__path_with_namespace

    @property
    def web_url(self):
        return f"{self.__gl.url}{self.__path_with_namespace}"

    @property
    def id(self):
        return self.__id

    @property
    def default_branch(self):
        return self.__default_branch


class ProjectManager(mock.MagicMock):
    def __init__(self, _parent=None, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.__parent = _parent
        self.__projects = {}

    def get(self, name):
        if name not in self.__projects:
            self.__projects[name] = Project(name, self, self.__parent)
        return self.__projects[name]
