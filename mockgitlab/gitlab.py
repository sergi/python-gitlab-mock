#!/usr/bin/env python3.9

# Copyright (C) 2023 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2023 Collabora Ltd"

import logging
from .projects import ProjectManager
from unittest import mock

bold = '\033[1m'
end = '\033[0m'


class Gitlab:
    _instances: dict = None

    def __new__(cls, url, *args, **kwargs):
        if cls._instances is None:
            cls._instances = {}
        if url not in cls._instances.keys():
            new_instance = _Gitlab(url, *args, **kwargs)
            cls._instances[url] = new_instance
        return cls._instances[url]


class _Gitlab(mock.MagicMock):

    def __init__(self, url=None, private_token=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._logger = logging.Logger("bar")
        channel = logging.StreamHandler()
        log_format = f'%(levelname)s - {bold}Gitlab{end} - %(message)s'
        formatter = logging.Formatter(log_format)
        channel.setFormatter(formatter)
        self._logger.addHandler(channel)
        self._logger.setLevel(logging.DEBUG)
        if url is not None:
            if url[-1] != '/':
                self.__url = f"{url}/"
            else:
                self.__url = url
        self.private_token = private_token
        self.projects = ProjectManager(self)

    @property
    def url(self):
        return self.__url

    @property
    def user(self):
        class UserName:
            @property
            def username(self):
                return "barfoo"
        return UserName()

    def debug(self, msg):
        self._logger.debug(msg)
