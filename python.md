# Python environment

## Python using virtualenv

Based on python 3.9, one can setup a virtual environments. 

```bash
$ pip3 -V
pip 20.3.4 from /usr/lib/python3/dist-packages/pip (python 3.9)
$ python3 -m pip install virtualenv
```

A virtual environment can be created in order to use this module: 

```bash
$ python3 -m virtualenv gitlab-mock.venv
$ source gitlab-mock.venv/bin/activate
(...)
$ python -m pip install pytest ruamel.yaml
$ pip list
Package          Version
---------------- -------
attrs            22.2.0
exceptiongroup   1.1.0
iniconfig        2.0.0
packaging        23.0
pip              22.3.1
pluggy           1.0.0
pytest           7.2.1
ruamel.yaml      0.17.21
ruamel.yaml.clib 0.2.7
setuptools       67.0.0
tomli            2.0.1
wheel            0.38.4
```

To reproduce the same virtual environment that this tools is being tested:

```bash
$ pip install -r requirements.txt
```

### Export changes on the environment

In case some newer packages are required, the easiest way to mark for others 
to use is:

```bash
pip freeze --all > requirements.txt
```

