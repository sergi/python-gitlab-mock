# python-gitlab-mock

> **Important Note**: this project will be rebuild. We thought on a better approach. 
> There is an [issue](https://gitlab.freedesktop.org/sergi/python-gitlab-mock/-/issues/1)
> opened for this effort.

This project was made to reproduce the module 
[python-gitlab](https://python-gitlab.readthedocs.io/en/stable/index.html)
interface for coverage testing purposes. It started as a directory in 
[ci-uprev](https://gitlab.freedesktop.org/sergi/ci-uprev),
(in fact, from the [attic ci-uprev](https://gitlab.freedesktop.org/sergi/attic_ci-uprev)) 
until we realized that it can be used in other modules for the unit testing, 
so it can be an independent module by itself.

There are a great number of features that make this tool optimal to test 
MesaCI's gitlab. Ideally, this should change in the future and become much more 
generic. This project is open to receive contributions with merge requests.

## Usage

By now this code uses `setuptools` to prepare a python package that can be 
installed in a virtual environment (or a `conda` one), so it can be imported by 
the tests in the module that requires it. Instructions to set up this virtual 
environment can be found in [python.md](./python.md).

Then, when gitlab objects are requested, they are created as mocks without 
accessing the real server.
```python
from mockgitlab import Gitlab
from mockgitlab.generators import (create_random_fake_token,
                                   create_random_fake_sha1_hash)
gl = Gitlab(url='https://gitlab.freedesktop.org', 
            private_token=create_random_fake_token())
project = gl.projects.get("barfoo/virglrenderer")
branch = project.branches.get("feature_request")
commit = project.commits.get(create_random_fake_sha1_hash())
file = project.files.get(".gitlab-ci.yml", ref=create_random_fake_sha1_hash())
issue = project.issues.get(1234)
job = project.jobs.get(12345678)
mergerequest = project.mergerequests.get(1234)
pipeline = project.pipelines.get(123456)
```

This module itself, has been created with testing in mind. So there are a bunch 
of tests in the `tests` directory that `pytest` can use. Once the 
module is installed in an environment, one can call `pytest -s` if the log information is 
interesting.
